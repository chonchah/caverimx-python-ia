import numpy as np

sigmoid=lambda x: 1.0/(1+ np.exp(-x))
ratios = lambda vector: (vector[i]/vector[i-1] for i in range(1,len(vector)))
sigmoid_derivative = lambda x: x * (1.0 - x)

class NeuralNetwork:
    def __init__(self, x, y, size=4):
        
        self.input      = x
        self.size       = size
        self.weights1   = np.random.rand(self.input.shape[1],self.size) 
        self.weights2   = np.random.rand(self.size,1)                 
        self.y          = y
        self.output     = np.zeros(self.y.shape)

    def feedforward(self):
        self.layer1 = sigmoid(np.dot(self.input, self.weights1))
        self.output = sigmoid(np.dot(self.layer1, self.weights2))
    def evaluar(self, entrada):
        salida = sigmoid(np.dot(entrada,self.weights1))
        salida = sigmoid(np.dot(salida, self.weights2))
        return salida

    def backprop(self):
        # application of the chain rule to find derivative of the loss function with respect to weights2 and weights1
        d_weights2 = np.dot(self.layer1.T, (2*(self.y - self.output) * sigmoid_derivative(self.output)))
        d_weights1 = np.dot(self.input.T,  (np.dot(2*(self.y - self.output) * sigmoid_derivative(self.output), self.weights2.T) * sigmoid_derivative(self.layer1)))

        # update the weights with the derivative (slope) of the loss function
        self.weights1 += d_weights1
        self.weights2 += d_weights2
