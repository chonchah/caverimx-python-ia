import numpy as np
from Neurona import NeuralNetwork
activacion_f = lambda x: x>0.7890009
global pruebas

def or_f( en ):
    if len(en)<2:
        raise ('No seas pendejo!')
    r = bool(en[0])
    for x in range(1, len(en)):
        r = r or bool(en[x])
    return r
def bites(n):
    v=[]
    while n>0:
        v.append(n%2)
        n//=2
    p = list(reversed(v))
    if len(p)<4: p = [0]*(4-len(p))+p
    return p
def prueba():
    X = np.array([[0,0,0,0],
                  [0,0,1,0],
                  [0,1,0,0],
                  [0,1,1,1],
                  [0,0,1,1],
                  [0,0,0,1]])
    y = np.array([[0],[1],[1],[1],[1],[1]])
    nn = NeuralNetwork(X,y)

    


    for i in range(10):
        #print ('iteracion', i, nn.weights1, sep='\n\n')
        #print (nn.weights2, sep='\n\n')
        nn.feedforward()
        #print (nn.layer1, sep='\n\n')        
        nn.backprop()
        
    
    _p=np.array([ [or_f(p)] for p in pruebas])
    _P=nn.evaluar(np.array(pruebas))
    P= activacion_f( _P )
    fallos=0
    for i, j in zip(_p, P):
        if i[0]!=j[0]: 
            fallos+=1
    
    #print (nn.output, np.array(list(zip(_P, P))), sep='\n\n')
    return fallos
pruebas = [ bites(i) for i in range(16)  ]
F=0
if __name__ == "__main__":
    for i in range(1000):
        fallos = prueba()
        F+=fallos
print ((1-F/16000)*100,'%')